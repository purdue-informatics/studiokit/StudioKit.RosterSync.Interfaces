﻿namespace StudioKit.RosterSync.Interfaces
{
	public interface IMembership
	{
		int GroupId { get; set; }
		string UserId { get; set; }
		string RoleId { get; set; }
		string ExternalId { get; set; }
	}
}