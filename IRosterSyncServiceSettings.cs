﻿namespace StudioKit.RosterSync.Interfaces
{
	public interface IRosterSyncServiceSettings
	{
		bool RosterSyncEnabled { get; set; }
		string RosterSyncUsername { get; set; }
		string RosterSyncPassword { get; set; }
		string RosterSyncEndPointUrl { get; set; }
	}
}