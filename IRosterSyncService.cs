﻿using StudioKit.Data.Entity.Identity.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.RosterSync.Interfaces
{
	public interface IRosterSyncService<TGroup, TMembership, TGroupCourseSection, TTerm, TContext, TUser>
		where TGroup : class, IGroup<TMembership, TGroupCourseSection>, new()
		where TMembership : class, IMembership, new()
		where TGroupCourseSection : class, IGroupCourseSection, new()
		where TTerm : class, ITerm, new()
		where TContext : IRosterSyncDbContext<TGroup, TMembership, TGroupCourseSection, TTerm>
		where TUser : class, IUser, new()

	{
		Task<IEnumerable<ICourseSection>> GetInstructorScheduleAsync(string instructorId, string termCode, CancellationToken cancellationToken);

		Task<IEnumerable<ICourseSection>> GetInstructorScheduleAsync(string instructorId, string termCode);

		Task<ICourseSection> GetCourseSectionAsync(int classId, CancellationToken cancellationToken);

		Task<ICourseSection> GetCourseSectionAsync(int classId);

		Task SyncGroupAsync(int groupId, string memberRoleId, CancellationToken cancellationToken);

		Task SyncGroupAsync(int groupId, string memberRoleId);

		Task SyncAllGroupsAsync(string memberRoleId, CancellationToken cancellationToken);

		Task SyncAllGroupsAsync(string memberRoleId);

		Task SyncTermsAsync(CancellationToken cancellationToken);

		Task SyncTermsAsync();
	}
}