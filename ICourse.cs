﻿namespace StudioKit.RosterSync.Interfaces
{
	public interface ICourse
	{
		string SubjectArea { get; set; }
		string CourseNumber { get; set; }
		string CourseTitle { get; set; }
	}
}