﻿using System;

namespace StudioKit.RosterSync.Interfaces
{
	public interface ITerm
	{
		string TermCode { get; set; }
		string Name { get; set; }
		DateTime StartDate { get; set; }
		DateTime EndDate { get; set; }
	}
}