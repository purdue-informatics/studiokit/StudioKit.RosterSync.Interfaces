﻿using System.Collections.Generic;

namespace StudioKit.RosterSync.Interfaces
{
	public interface ICourseSection
	{
		int Id { get; set; }
		string SectionNumber { get; }
		string Subpart { get; set; }
		IEnumerable<ICourse> Courses { get; set; }
		string Fullpart { get; }
	}
}